
public class Couleur {
	//List of Strings symbolising colors
	public final static String[] LISTE_COULEUR_STRINGS = {"WHITE","BLACK","RED","GREEN","BLUE","YELLOW","MAGENTA","CYAN"};
	
	private int red=0;
	private int green=0;
	private int blue=0;
	private int alpha=255;
	
	//Create a Couleur with an int given in parameter
	public Couleur(int color) {
		this.blue = color & 0xff;
 		this.green = (color & 0xff00) >> 8;
 		this.red = (color & 0xff0000) >> 16;
 		this.alpha =(color & 0xff000000) >> 24;
	}
	
	//Create a Couleur with a String given in parameter
	public Couleur(String nom) {
	
		switch(nom) {
		case "WHITE":
			this.red=255;
			this.green=255;
			this.blue=255;
			break;
		case "BLACK":
			break;
		case "RED":
			this.red=255;
			break;
		case "GREEN":
			this.green=255;
			break;
		case "BLUE":
			this.blue=255;
			break;
		case "YELLOW":
			this.red=255;
			this.green=255;
			break;
		case "CYAN":
			this.green=255;
			this.blue=255;
			break;
		case "MAGENTA":
			this.blue=255;
			this.red=255;
			break;
		default:
			System.out.println("Couleur non reconnue");
		}
		
	}

	//Set values of the Couleur using a new int
	public void setCouleur(int color) {
		this.blue = color & 0xff;
 		this.green = (color & 0xff00) >> 8;
 		this.red = (color & 0xff0000) >> 16;
 		this.alpha =(color & 0xff000000) >> 24;
	}
	
	@Override
	public String toString(){
		return this.red+"."+this.green+"."+this.blue;
	}
	
	//Return an int corresponding at the Couleur 
	public int toValidColor() {
		return (this.alpha << 24)
			| (this.red << 16)
			| (this.green << 8)
			| (this.blue << 0);
	}
	
	//Transform the Couleur in grey level
	public void toNiveauGris() {
		int moyenne =this.calculMoyenne();
		this.blue=moyenne;
		this.red=moyenne;
		this.green=moyenne;
	}
	
	//Transform a Couleur in his binary form, using Couleurs given in parameters
	public void toBinaire(Couleur c1,Couleur c2) {
		if (this.calculMoyenne()>128) {
			this.green=c1.getGreen();
			this.blue=c1.getBlue();
			this.red=c1.getRed();
		}else {
			this.green=c2.getGreen();
			this.blue=c2.getBlue();
			this.red=c2.getRed();
		}
	}
	
	//Tranform the Couleur in his opposite
	public void toNegative() {
		//System.out.println(this.blue);
		this.blue=255-this.blue;
		this.green=255-this.green;
		this.red=255-this.red;
		
	}
	
	public int getRed() {
		return this.red;
	}
	
	public int getGreen() {
		return this.green;
	}

	public int getBlue() {
		return this.blue;
	}
	
	//Calculate the average of the Couleur attribute (except alpha)
	public int calculMoyenne() {
		return (this.green+this.red+this.blue)/3;
	}
	
	//Return the understandable static final list of Couleur
	public static String choixPossibles() {
		String retour="";
		for(int i=0;i<Couleur.LISTE_COULEUR_STRINGS.length;i++) {
			retour+=Couleur.LISTE_COULEUR_STRINGS[i].toString()+" : "+(i+1)+" / ";
		}
		return retour;
	}
}
