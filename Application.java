import java.io.File;
import java.io.IOException;

import java.util.Scanner;

public class Application {

	public static void main(String[] args) throws IOException {
		
	//Initializing of the scanner and path to the image
		Scanner entree = new Scanner(System.in);
		System.out.println("Entrez le chemin d'acc�s au dossier qui contient l'image (ex : C:\\Users\\...)");
		System.out.println("ATTENTION l'image doit s'appeler \"image\" et doit etre situ� dans ce dossier ");
		String name = entree.nextLine();
		System.out.println("Veuillez renseigner le nom de l'image (nom.jpg, nom.png, ...");
		String ext = entree.nextLine();
		
	//Loading of the image in a File, if it fail to load, the program exit
		File image = new File(name+"\\"+ext);
		if(!image.exists()) {
			System.out.println("L'image n'a pas pu �tre charg�e");
		}else {
			System.out.println("Image charg�e");
			Picture monImage = new Picture();
			monImage.charger(image);
			System.out.println("Que voulez vous faire avec cette image ?");
			System.out.println("La mettre en niveaux de gris ? : 1");
			System.out.println("La binariser ? : 2");
			System.out.println("La Wharoliser ? : 3");
			System.out.println("La rendre negative ? : 4");
			System.out.println("Attention, taper autre chose que ces chiffres annule l'op�ration");
			int choix = entree.nextInt();
			switch (choix) {
			case 1:
				monImage.niveauxDeGris();
				break;
			case 2:
				boolean continuer = false;
				int[] couls= {0,0};
				String[] numStr = {"primaire","secondaire"};
			//For each necessary Couleur, we ask the user to chose one in the static final List of Couleur, if the answer is not 
			//usable, it loop since it is
				for(int k=0;k<2;k++) {
					while (!continuer) {
						System.out.println("\n"+"\n"+"\n"+"\n"+Couleur.choixPossibles());
						System.out.println("Quelle couleur "+numStr[k]+" choisissez vous ? ");
						couls[k] = entree.nextInt();
						if (couls[k]>=1 && couls[k]<=8)
							continuer=true;
					}
					continuer=false;
				}
			//We binarize the Picture with Couleurs asked
				monImage.binariser(couls[0], couls[1]);
				break;
			case 3:
				continuer = false;
				int[] c = {0,0,0,0,0,0,0,0};
				String[] numString = {"PREMIERE","DEUXIEME","TROISIEME","QUATRIEME","CINQUIEME","SIXIEME","SEPTIEME","HUITIEME"};
			//For each necessary Couleur, we ask the user to chose one in the static final List of Couleur, if the answer is not 
			//usable, it loop since it is
				for (int k=0;k<c.length;k++) {
					while (!continuer) {
						System.out.println("\n"+"\n"+"\n"+"\n"+Couleur.choixPossibles());
						System.out.println("Quelle "+numString[k]+" COULEUR choisissez vous ? ");
						c[k] = entree.nextInt();
						if (c[k]>=1 && c[k]<=8)
							continuer=true;
					}
					continuer=false;
				}
			//We Wharolize the Picture with Couleurs asked
				monImage.wharoliser(c[0], c[1], c[2], c[3], c[4], c[5], c[6], c[7]);
				break;
			case 4:
			//We turn negative the Picture
				monImage.toNegative();
				break;
			default:
			}      
		//And we save the picture in the same folder that the original
			monImage.enregistrer(name,ext);
		}
		entree.close();
	}
	
}
